'use strict';

var _ = require('lodash');
var Characterclass = require('./characterclass.model');

// Get list of characterclasses
exports.index = function(req, res) {
  Characterclass.find(function (err, characterclasses) {
    if(err) { return handleError(res, err); }
    return res.json(200, characterclasses);
  });
};

// Get a single characterclass
exports.show = function(req, res) {
  Characterclass.findById(req.params.id, function (err, characterclass) {
    if(err) { return handleError(res, err); }
    if(!characterclass) { return res.send(404); }
    return res.json(characterclass);
  });
};

// Creates a new characterclass in the DB.
exports.create = function(req, res) {
  Characterclass.create(req.body, function(err, characterclass) {
    if(err) { return handleError(res, err); }
    return res.json(201, characterclass);
  });
};

// Updates an existing characterclass in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Characterclass.findById(req.params.id, function (err, characterclass) {
    if (err) { return handleError(res, err); }
    if(!characterclass) { return res.send(404); }
    var updated = _.merge(characterclass, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, characterclass);
    });
  });
};

// Deletes a characterclass from the DB.
exports.destroy = function(req, res) {
  Characterclass.findById(req.params.id, function (err, characterclass) {
    if(err) { return handleError(res, err); }
    if(!characterclass) { return res.send(404); }
    characterclass.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}