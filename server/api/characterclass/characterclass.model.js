'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CharacterClassSchema = new Schema({
    name: {type: String,
        required: 'Please enter a class name',
        trim: true
    },
    eTimer: {
        type: Number,
        required: 'Please enter a duration for the energy timer'
    },
    sTimer: {
        type: Number,
        required: 'Please enter a duration for the stamina timer'
    },
    hTimer: {
        type: Number,
        required: 'Please enter a duration for the honor timer'
    }
});

module.exports = mongoose.model('CharacterClass', CharacterClassSchema);