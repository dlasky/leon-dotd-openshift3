'use strict';

var _ = require('lodash');
var Guide = require('./guide.model');

// Get list of guides
exports.index = function(req, res) {
  Guide.find(function (err, guides) {
    if(err) { return handleError(res, err); }
    return res.json(200, guides);
  });
};

// Get a single guide
exports.show = function(req, res) {
  Guide.findById(req.params.id, function (err, guide) {
    if(err) { return handleError(res, err); }
    if(!guide) { return res.send(404); }
    return res.json(guide);
  });
};

// Creates a new guide in the DB.
exports.create = function(req, res) {
  Guide.create(req.body, function(err, guide) {
    if(err) { return handleError(res, err); }
    return res.json(201, guide);
  });
};

// Updates an existing guide in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Guide.findById(req.params.id, function (err, guide) {
    if (err) { return handleError(res, err); }
    if(!guide) { return res.send(404); }
    var updated = _.merge(guide, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, guide);
    });
  });
};

// Deletes a guide from the DB.
exports.destroy = function(req, res) {
  Guide.findById(req.params.id, function (err, guide) {
    if(err) { return handleError(res, err); }
    if(!guide) { return res.send(404); }
    guide.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}