'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var GuideSchema = new Schema({
    name: {
        type: String,
        require: 'Please enter a guide name',
        trim: true
    },
    url: {
        type: String,
        require: 'Please enter a guide url',
        trim: true
    },
    description: {
        type: String,
        require: 'Please enter a guide description',
        trim: true
    },
    order: Number,
    visible: Boolean
});

module.exports = mongoose.model('Guide', GuideSchema);