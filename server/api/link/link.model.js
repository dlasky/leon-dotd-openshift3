'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LinkSchema = new Schema({
    category: {
        type: String,
        required: 'Please enter a category name',
        trim: true
    },
    linkText: {
        type: String,
        required: 'Please enter link text',
        trim: true
    },
    linkUrl: {
        type: String,
        required: 'Please enter the link url',
        trim: true
    },
    description: String,
    visible: Boolean
});

module.exports = mongoose.model('Link', LinkSchema);