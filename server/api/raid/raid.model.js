'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RaidSchema = new Schema({
    name: {
        type: String,
        required: 'Please enter a Raid name',
        trim: true
    },
    shortName: {
        type: String,
        trim: true
    },
    ugupId: {
        type: Number
    },
    timer: {
        type: Number,
        required: 'Please enter a Timer number (hours)'
    },
    cooldown: {
        type: Number
    },
    slots: {
        type: Number,
        required: 'Please enter number of slots'
    },
    size: {
        type: String,
        require: 'Please enter a raid size',
        trim: true
    },
    sizeId: {
        type: Number
    },
    zoneId: {
        type: String,
        trim: true
    },
    zoneName: {
        type: String,
        trim: true
    },
    tiered: {
        type: Boolean
    },
    image: {
        type: String
    },
    icon: {
        type: String
    },
    notes: [NotesSchema],
    classes: [ClassSchema],
    difficulties: [
        {
            difficulty: {
                type: String,
                required: 'Please enter Difficulty',
                trim: true
            },
            health: {
                type: Number
            },
            ap: {
                type: Number
            },
            fs: {
                type: Number
            },
            tiers: [TierSchema]
        }
    ]
});

var TierSchema = new Schema({
    name: {
        type: String,
        trim: true
    },
    note: {
        type: String,
        trim: true
    },
    value: {
        type: Number
    }
}, {_id: false});

var ClassSchema = new Schema({
    name: {
        type: String,
        trim: true
    }
}, {_id: false});

var NotesSchema = new Schema({
    note: {
        type: String,
        trim: true
    }
}, {_id: false});

module.exports = mongoose.model('Raid', RaidSchema);