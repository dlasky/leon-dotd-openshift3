'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ScriptSchema = new Schema({
    version: {
        type: String,
        required: 'Please enter a version number',
        trim: true
    },
    file: {
        type: String,
        required: 'Please enter a file location',
        trim: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    commands: [Command],
    changes: [Change]
});

var Command = new Schema({
    command: {
        type: String,
        required: 'Please enter a command',
        trim: true
    },
    description: {
        type: String,
        required: 'Please enter a command description',
        trim: true
    },
    active: {
        type: Boolean
    }
}
);

var Change = new Schema({
    description: {
        type: String,
        required: 'Please enter a change description',
        trim: true
    }
});

module.exports = mongoose.model('Script', ScriptSchema);