var fs = require('fs');

var leon_raids = JSON.parse(fs.readFileSync('raids_raw.json', 'utf-8'));
var ugup_raids = JSON.parse(fs.readFileSync('ugup-raids.json', 'utf-8'));

var raid_file = 'merged_raids.json';
var merged_raids = [];


function findWithAttr(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
}

function get5pgSize(size){
	switch(size) {
		case 0:
			return 'Small';
			break;
		case 1:
			return 'Medium';
			break;
		case 2:
			return 'Large';
			break;
		case 3:
			return 'Epic';
			break;
		case 4: 
			return 'Colossal';
			break;
		case 5:
			return 'Personal';
			break;
		case 6: 
			return 'Gigantic';
			break;
		default:
			return 'Unknown';
	}
}

function get5pgDifficulty(diff){
	switch(diff) {
		case 1:
			return 'Normal';
			break;
		case 2:
			return "Hard";
			break;
		case 3:
			return "Legendary";
			break;
		case 4:
			return "Nightmare";
			break;
	}
}

for(var i=0;i<ugup_raids.length;i++){
	var merged = {};
	var leon_raid;
	var raid = ugup_raids[i];
	leon_index = findWithAttr(leon_raids, 'name',raid.name);
	if(leon_index !== undefined) {
		leon_raid = leon_raids[leon_index];
	} else {
		leon_raid = false;
	}

	merged.name = raid.name;
	merged.shortName = raid.shortname;
	merged.ugupId = raid.id;
	merged.raidTimer = raid.raidtimer;
	merged.cooldown = raid.cooldowntimer;
	merged.sizeId = raid.size+1,
	merged.size = (leon_raid)? leon_raid.size : get5pgSize(raid.size);
	if(leon_raid){
		merged.zoneId = leon_raid.zoneNumber;
		merged.zoneName = leon_raid.zoneName;
	} else {
		merged.zoneId = '';
		merged.zoneName = '';
	}
	merged.slots = raid.maxattackers;
	merged.image = raid.image;
	merged.icon = raid.icon;
	merged.notes = '';
	var classes = [];
	if(leon_raid){
		for(var j=0; j<leon_raid.classes.length; j++){
			classes.push({"name": leon_raid.classes[j]});
		}
	}
	merged.classes = classes;

	var diffs = [];
	for(var k=0; k<raid.difficulty.length; k++){
		var diff = {};
		diff.difficultyId = raid.difficulty[k].level;
		diff.difficulty = get5pgDifficulty(raid.difficulty[k].level);
		diff.health = raid.difficulty[k].health;
		diff.ap = (raid.difficulty[k].level === 4) ? raid.difficulty[k].health/raid.maxattackers/2 : 0;
		diff.fs = raid.difficulty[k].health/raid.maxattackers;
        diff.tiers = [];
		if(leon_raid && leon_raid.difficulties[k] !== undefined ){
            for(var tier in leon_raid.difficulties[k].tiers){
                diff.tiers.push({name: tier, value: leon_raid.difficulties[k].tiers[tier]});
            }
			//diff.tiers = leon_raid.difficulties[k].tiers;
		}
        diffs.push(diff);
	}
	merged.difficulties = diffs;

	merged_raids.push(merged);
}

    fs.writeFile(raid_file, JSON.stringify(merged_raids), function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log("Raid JSON saved to " + raid_file);
        }
    });
