var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/dotd-dev');

var ScriptModel = require('../api/script/script.model');

var fs = require('fs');

var scripts = JSON.parse(fs.readFileSync('scripts.json', 'utf-8'));
ScriptModel.create(scripts, function(err, response){
    if(err){
        console.log(err);
    }
});